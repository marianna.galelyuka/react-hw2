import React from "react";

import Header from "./components/header";
import ProductList from './components/productList';

import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      cart: [],
      favorites: [],
    };
    this.addToCart = this.addToCart.bind(this);
    this.addToFavorites = this.addToFavorites.bind(this);
  }

  componentDidMount() {
    fetch('products.json')
    .then(res => res.json())
    .then((products) => this.setState({products: products}))
  }

  componentDidUpdate() {
    localStorage.setItem('favorites', JSON.stringify(this.state.favorites));
    localStorage.setItem('cart', JSON.stringify(this.state.cart));
  }

  addToCart(product) {
    this.setState({ cart: [...this.state.cart, product] });
  }

  addToFavorites(product) {
    let isInFavorites = false;
    this.state.favorites.forEach((item) => {
      if (item.id === product.id) {
        isInFavorites = true;
      }
    });

    (!isInFavorites) ?
      this.setState({ favorites: [...this.state.favorites, product] }) :
        this.deleteFromFavorites(product)
  }

  deleteFromFavorites(product) {
    this.setState({ favorites: this.state.favorites.filter((item) => item.id!== product.id) });
  }

  render() {
    return (
      <div className="App">
        <Header cart={this.state.cart} favorites={this.state.favorites}/>
        <ProductList products={this.state.products} onAddToCart={this.addToCart} onAddToFavorites={this.addToFavorites} />
      </div>
    );
  }
}

export default App;