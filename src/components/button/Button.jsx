import React from "react";
import PropTypes from "prop-types";

class Button extends React.Component {
    render() {
        return (
            <button className={this.props.className} style={{backgroundColor: this.props.backgroundColor}} onClick={this.props.onClick}>
                {this.props.text}
            </button>
        )
    }
}

Button.propTypes = {
    onClick: PropTypes.func,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    className: PropTypes.string,
};

Button.defaultProps = {
    backgroundColor: "rgb(50, 150, 150)",
};

export default Button;