import React from 'react';
import Product from '../product';
import PropTypes from 'prop-types';

import styles from './ProductList.module.css';

class ProductList extends React.Component {
    render() {
        return (
            <main>
                <h1 className={styles.main__title}>Our products</h1>
                <div className={styles.productList}>
                    {this.props.products.map((el) => (
                        <Product product={el} key={el.id} onAddToCart={this.props.onAddToCart} onAddToFavorites={this.props.onAddToFavorites} />
                    ))}
                </div>
            </main>
        )
    }
}

ProductList.propTypes = {
    products: PropTypes.array,
    onAddToCart: PropTypes.func,
    onAddToFavorites: PropTypes.func
}

export default ProductList;