import React from 'react';
import Button from '../button';
import Modal from '../modal';
import PropTypes from 'prop-types';

import { FaStar } from "react-icons/fa";

import styles from "./Product.module.css";

class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            isFavorite: false,
        }
    }

    setModalOpen = () => {
        this.setState ({
            isModalOpen: (!this.state.isModalOpen)
        });
    }
    
    hideModal = () => {
        this.setState ({
            isModalOpen: false
        });
    }

    setIsFavorite = () => {
        this.setState({isFavorite: (!this.state.isFavorite)});
        this.props.onAddToFavorites(this.props.product);
    }

    setInCart = () => {
        this.props.onAddToCart(this.props.product);
        this.setState ({ isModalOpen: (!this.state.isModalOpen) });
    }

    render() {
        return (
            <div className={styles.product}>
                <img className={styles.product__img} src={this.props.product.imgUrl} alt="Product" />
                <FaStar id={this.props.product.id} onClick={this.setIsFavorite} className={`${styles.product__favIcon} ${this.state.isFavorite && `${styles.active}`}`} />
                <h4 className={styles.product__title}>{this.props.product.productName}</h4>
                <div className={styles.product__details}>
                    <p className={styles.product__details_color}> {this.props.product.color}</p>
                    <p className={styles.product__details_price}> UAH {this.props.product.price}</p>
                </div>
                <Button text="Add to cart" className={styles.product__toCartBtn} onClick={this.setModalOpen} />

                <div className='Modal' onClick={this.hideModal}>
                    <Modal header="Do you want to add this product to a cart?" text="Adding a product to a cart doesn't mean its reservation. Do you want to continue?" actions={<><Button text='Ok' className={styles.product__toCartBtn_confirm} onClick={this.setInCart}/><Button text="Cancel" className={styles.product__toCartBtn_cancel} onClick ={this.hideModal} /></>} isOpen={this.state.isModalOpen} />
                </div>
            </div>
        )
    }
}

Product.propTypes = {
    isFavorite: PropTypes.bool,
    product: PropTypes.object,
    imgUrl: PropTypes.string,
    id: PropTypes.number,
    productName: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.number,
};

export default Product;