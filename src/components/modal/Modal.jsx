import React from "react";
import Button from '../button';
import PropTypes from "prop-types";

import styles from './Modal.module.css';

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            isCloseButton: false,
        };
    }

    static getDerivedStateFromProps(props, state) {
        return { isModalOpen: props.isOpen, isCloseButton: props.closeButton};
    }

    render() {
        return (
            this.state.isModalOpen ? 
                this.state.isCloseButton ?
                    <div className={styles.modal__content} >
                        <div className={styles.container} onClick={e => e.stopPropagation()}>
                            <div className={styles.modal__title}>
                                <h3 className={styles.modal__header}>{this.props.header}</h3>
                                <Button text="X" />
                            </div>
                            <p className={styles.modal__text}>{this.props.text}</p>
                            <div className={styles.modal__buttons}>
                                {this.props.actions}
                            </div>
                        </div>
                    </div>
                    :
                    <div className={styles.modal__content} >
                        <div className={styles.container} onClick={e => e.stopPropagation()}>
                            <div className={styles.modal__title}>
                                <h3 className={styles.modal__header}>{this.props.header}</h3>
                            </div>
                            <p className={styles.modal__text}>{this.props.text}</p>
                            <div className={styles.modal__buttons}>
                                {this.props.actions}
                            </div>
                        </div>
                    </div>
                : null
        )
    }
}

Button.propTypes = {
    isOpen: PropTypes.bool,
    closeButton: PropTypes.bool,
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.object,
};

Button.defaultProps = {
    closeButton: false,
};

export default Modal;