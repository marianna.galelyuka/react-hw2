import React from "react";
import PropTypes from "prop-types";

import { FaStar, FaShoppingCart } from "react-icons/fa"

import styles from './Header.module.css'; 

class Header extends React.Component {
    render() {
        return (
            <header className={styles.header}>
                <div className={styles.header__logo}>
                    <a className={styles.header__logo_link} href="/">
                        <img alt="isweet.com.ua" className={styles.header__logo_img} width="200" height="100" src="https://isweet.com.ua/content/images/2/200x100l90nn0/75227810382232.webp"/>
                    </a>
                </div>
                <div className={styles.header__icons}>
                    <div className={styles.header__icon_fav}>
                        {this.props.favorites.length === 0 ?
                            <FaStar className={styles.icon__fav} /> :
                            <FaStar className={styles.icon__fav_active}/>
                        }
                        <span className={styles.icon__fav_number}>{this.props.favorites.length}</span>
                    </div>

                    <div className={styles.header__icon_cart}>
                        {this.props.cart.length === 0 ?
                            <FaShoppingCart className={styles.icon__cart} /> :
                            <FaShoppingCart className={styles.icon__cart_active}/>
                        }
                        <span className={styles.icon__cart_number} >{this.props.cart.length}</span>
                    </div>
                </div>
            </header>
        );
    }
}

Header.propTypes={
    favorites: PropTypes.array,
    cart: PropTypes.array
}

export default Header;